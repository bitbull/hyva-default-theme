# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-02-15
### Added
- Initial Release added

### Changed
- see [1.0.0]

### Removed
- none

# Beta releases
#### [0.2.0] - 2021-02-03
#### [0.1.0] - 202-12-09

[Unreleased]: https://gitlab.hyva.io/hyva-themes/magento2-default-theme/-/compare/1.0.0...master
[1.0.0]: https://gitlab.hyva.io/hyva-themes/magento2-default-theme/-/compare/0.2.0...1.0.0
[0.2.0]: https://gitlab.hyva.io/hyva-themes/magento2-default-theme/-/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.hyva.io/hyva-themes/magento2-default-theme/-/tags/0.1.0
